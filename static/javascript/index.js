"use strict";

// VAKIOT

const googleURL = "https://google.com";
const jokuKuva = "./static/images/pexels-quang-nguyen-vinh-2175952.jpg";
// vaihda-komento -> body-elementin tausta punaiseksi, lisätään (tai poistetaan) luokka body-elementille
const taustaLuokka = "taustaON";

// ELEMENTIT

let kontti = document.createElement("div");
kontti.id = "kontti";

document.body.appendChild(kontti);

let ylarivi = document.createElement("div");
ylarivi.id = "ylakolumni";

kontti.appendChild(ylarivi);

let kentta = document.createElement("input");
kentta.id = "kentta";
kentta.value = "listaa"; // kentän (input) oletusarvoksi listaa

let nappi = document.createElement("button");
nappi.id = "nappi";

let sisalto = document.createTextNode("Suorita");

ylarivi.appendChild(kentta);

ylarivi.appendChild(nappi);

nappi.appendChild(sisalto);

let alarivi = document.createElement("div");
alarivi.id = "alakolumni";

kontti.appendChild(alarivi);

let alavasen = document.createElement("div");
alavasen.id = "alavasen";

alarivi.appendChild(alavasen);

let alaoikea = document.createElement("div");
alaoikea.id = "alaoikea";

alarivi.appendChild(alaoikea);

// FUNKTIOT - TEHTÄVÄN KOMENNOT

// objekti funktioille ja ominaisuuksille
const komennot = {};

/*
  kaikki-ominaisuus ei tule komennot-objektin avaimeksi näin asetettuna
  palauttaa objektin kaikki funktiot, mutta ei ominaisuuksia
  funktiot lisätään jäljempänä muodossa objekti.ominaisuus = funktio;

  kaikki-ominaisuudella on vain get-aksessori
*/
Object.defineProperty(komennot, "kaikki", {
  get() {
    return Object.keys(this); // palauttaa taulukon
  },
});

/*
  huom. komennot-objektin funktiot alustettava ennen käyttöä
  
  Annetaan funktioiden nimeksi komentoa vastaava nimi -> saadaan suoritettua komento objektin avaimen kautta -> komennot[avain](args)
*/

// 2.
// komennot-objektin avaimeksi tulee 'google'
komennot.google = (vanhempi) => {
  let a = document.createElement("a");
  a.textContent = "Siirrytäänkö googleen";
  a.href = googleURL;
  a.target = "_BLANK"; // uuteen välilehteen
  vanhempi.appendChild(a);
};

// 3.
komennot.listaa = (vanhempi) => {
  let ul = document.createElement("ul");
  vanhempi.appendChild(ul);
  for (let komento of komennot.kaikki) {
    let li = document.createElement("li");
    ul.appendChild(li);
    let a = document.createElement("a");
    a.textContent = komento;
    li.appendChild(a);
    // tekstiä klikattaessa
    a.onclick = ({ target }) => {
      // klikattu teksti input-elementin kentän arvoksi
      kentta.value = target.textContent;
      if (target.textContent != "vaihda" && UI.tausta) {
        UI.tausta = false;
      }
      // jos value-arvo on 'listaa', paikaksi tulee alavasen muutoin alaoikea
      let paikka = komento == "listaa" ? alavasen : alaoikea;
      if (paikka.firstElementChild) {
        if (komento == "listaa" && alaoikea.firstElementChild) {
          UI.poistaLapset(alaoikea);
        }
        UI.poistaLapset(paikka);
      }
      // hakasulkeet -> komennot-objektin avain, sulut perässä -> komentoa vastaavan funktion suoritus,
      komennot[komento](paikka);
    };
  }
};

// 4.
komennot["Markku Palomäki"] = (vanhempi) => {
  let img = document.createElement("img");
  img.src = jokuKuva;
  // img.setAttribute("src", jokuKuva);
  img.width = 300;
  img.height = 300;
  vanhempi.appendChild(img);
};

// 5.
// luku oletusarvona 100 riviä tekstiä
komennot["100"] = (vanhempi, luku = 100) => {
  let div = document.createElement("div");
  let s = "";
  vanhempi.appendChild(div);

  for (let i = 0; i < luku; i++) {
    s = i + 1 + ". generoitua tekstiä" + "<br>"; // järjestysnumero muodostuu silmukan laskurinumerosta (i) lisättynä yhdellä (alkaa nollasta)
    div.innerHTML += s;
  }
};

// 6.
komennot.piirrä = (vanhempi) => {
  const canvas = document.createElement("canvas");
  canvas.id = "piirto";
  canvas.width = 300;
  canvas.height = 300;
  canvas.style.backgroundColor = "#fff";
  vanhempi.appendChild(canvas);
  const ctx = canvas.getContext("2d");
  ctx.beginPath();
  ctx.strokeStyle = "#000";
  ctx.lineWidth = 2;
  ctx.arc(150, 150, 100, 0, Math.PI * 2, true);
  ctx.stroke();
};

// 7.
komennot["tee popup"] = (vanhempi) => {
  let div = document.createElement("div");
  let span = document.createElement("span");
  let button = document.createElement("button");

  div.setAttribute("id", "ilmestynyt");
  span.innerHTML = "Tässä <b>pyydetty</b> popup"; // b-elementillä lihavoitu tekstinosa
  button.textContent = "Poista ID";

  // alaoikealta (vanhempi) poistuu tämän funktion lisäämät elementit -> klikkaamalla div-elementtiä tai sen lapsi-elementtiä (button)
  div.onclick = (_) => UI.poistaLapset(vanhempi);

  vanhempi.appendChild(div);
  div.appendChild(span);
  div.appendChild(button);
};

// 8.
komennot.vaihda = () => {
  UI.tausta = true;
};

// 9.
komennot.funktio = (vanhempi) => {
  console.log("Funktio toimii");
  let alakentta = document.createElement("input");
  let alanappi = document.createElement("button");
  alakentta.id = "alakentta";
  alanappi.textContent = "Suorita";
  vanhempi.appendChild(alakentta);
  vanhempi.appendChild(alanappi);
  alanappi.addEventListener("click", handleButton2Click, false);
  alakentta.addEventListener("keydown", handleDownKeyInput, false);
};

// UI = user interface

let UI = {};

UI.poistaLapset = (vanhempi) => {
  while (vanhempi.firstElementChild) {
    vanhempi.removeChild(vanhempi.firstElementChild);
  }
};

/*
  tausta-ominaisuudella on sekä get- että set-aksessori
  set-aksessori hyväksyy boolean tyyppiset arvot false ja true
  get-aksessori palauttaa true jos tausta on vaihdettu ja punainen, muutoin false
*/
Object.defineProperty(UI, "tausta", {
  get() {
    // get palauttaa ominaisuuden arvon
    let luokat = document.body.classList;
    let taustaVaihdettu = false;
    if (luokat.contains(taustaLuokka)) {
      taustaVaihdettu = true;
    }
    return taustaVaihdettu;
  },
  set(value) {
    // asetetaan tausta punaiseksi -> joko true tai false
    let luokat = document.body.classList;
    if (value == false) {
      if (luokat.contains(taustaLuokka) == true) {
        luokat.remove(taustaLuokka);
      }
    } else if (value == true) {
      if (luokat.contains(taustaLuokka) == false) {
        luokat.add(taustaLuokka);
      }
    }
  },
});

// FUNKTIOT - EI KOMENTO

function tuntematonKomento() {
  let p = document.createElement("p");
  p.textContent = "Komentoa ei tunnistettu";
  alaoikea.appendChild(p);
}

// KÄSITTELIJÄT

// alakentän yhteinen käsittelijä, kun button-elementtiä klikataan tai input-kenttä aktiivisena painetaan enter-painiketta
function handleDownInput() {
  let alakentta = document.body.querySelector("#alakentta");
  switch (alakentta?.value) {
    case "":
      console.log("anna komento");
      break;
    case "reload":
      window.location.reload();
      break;
    default:
      console.log("Kirjoitit: " + alakentta.value);
      break;
  }
}

// käsittelijä funktio-komennon luoman (alaoikealle) button-elementin klikkaukselle
function handleButton2Click(_) {
  handleDownInput();
}

// käsittelijä funktio-komennon luoman (alaoikealle) input-elementin näppäimistön painalluksille
function handleDownKeyInput({ key }) {
  if (key == "Enter") {
    handleDownInput();
  }
}

// käsittelijäfunktion value-parametri on (mahdollisesti) suoritettavan komennon nimi
function handleInput(value) {
  if (alaoikea.firstElementChild) {
    UI.poistaLapset(alaoikea);
  }
  if (alavasen.firstElementChild) {
    UI.poistaLapset(alavasen);
  }
  // suoritetaan mahdollisesti toista komentoa ja tausta on edelleen punainen -> vaihdetaan taustan väri takaisin
  if (value != "vaihda" && UI.tausta) {
    UI.tausta = false;
  }

  // jos syöte ei ole numero -> isNaN (is not-a-number) palauttaa true
  if (isNaN(value) == false) {
    value = parseInt(value);
    if (1 <= value && value <= 999) {
      komennot["100"](alaoikea, value);
    } else {
      tuntematonKomento();
    }
  } else if (value.match(/markku palomäki/i)) {
    komennot["Markku Palomäki"](alaoikea);
  } else {
    // suoritetaan komento, jos ominaisuus löytyy komennot-objektista
    if (komennot.hasOwnProperty(value)) {
      // jos value-arvo on 'listaa', paikaksi tulee alavasen muutoin alaoikea
      let paikka = value == "listaa" ? alavasen : alaoikea;
      // tällä suoritetaan komento ja ilmoitetaan minne tulos laitetaan (paikka) funktion parametrina
      komennot[value](paikka);
    } else {
      tuntematonKomento();
    }
  }
  kentta.value = "";
}

// käsittelijä yläkentän napin klikkaukselle
const handleButtonClick = (_) => {
  handleInput(kentta.value);
};

// käsittelijä input-kentän enter-painallukselle
const handleKeyInput = ({ key }) => {
  if (key == "Enter") {
    handleInput(kentta.value);
  }
};

// KUUNTELIJAT

nappi.addEventListener("click", handleButtonClick, false);
kentta.addEventListener("keydown", handleKeyInput, false);
